package com.previred.java.presentacionjava11.varLambdas;

import java.util.function.IntFunction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jetbrains.annotations.NotNull;

/**
 *
 * @author mig_s
 */
public class Ejemplo01Lambdas {
    private static final Logger LOGGER = Logger.getLogger(Ejemplo01Lambdas.class.getName());

    public static void main(String args[]) {
        IntFunction<Integer> intMultiplica1 = (int x) -> x * 2;
        IntFunction<Integer> intMultiplica2 = (x) -> x * 2;
        
        IntFunction<Integer> intMultiplica3 = (var x) -> x * 2;
        IntFunction<Integer> intMultiplica4 = (@NotNull var x) -> x * 2;   // valor no permite nulos
        
        LOGGER.log(Level.INFO, "1 Multiplica 3 por 2 = {0}", intMultiplica1.apply(3));
        LOGGER.log(Level.INFO, "2 Multiplica 3 por 2 = {0}", intMultiplica2.apply(3));
        LOGGER.log(Level.INFO, "3 Multiplica 3 por 2 = {0}", intMultiplica3.apply(3));
        LOGGER.log(Level.INFO, "4 Multiplica 3 por 2 = {0}", intMultiplica4.apply(3));
        //LOGGER.log(Level.INFO, "Multiplica 3 por 2 = {0}", doubleIt4.apply(null));
        
        
        ITest multiply1 = (double x,double y) ->  x * y;
//        ITest multiply1_1 = (double x, y) ->  x * y;
        ITest multiply2 = (var x,var y) ->  x * y;
//        ITest multiply2_1 = (var x, y) ->  x * y;
//        ITest multiply2_2 = (var x,double y) ->  x * y;
        ITest multiply3 = (x,y) ->  x * y;
        ITest multiply4 = (@NotNull var x,var y) ->  x * y;
//        Function<Integer, Integer> doubleIt5 = (Integer x) -> x * 2;

        LOGGER.log(Level.INFO, "1 Multiplica 3 por 2 = {0}", multiply1.doMath(3,2));
        LOGGER.log(Level.INFO, "1 Multiplica 3 por 2 = {0}", multiply2.doMath(3,2));
        LOGGER.log(Level.INFO, "1 Multiplica 3 por 2 = {0}", multiply3.doMath(3,2));
        LOGGER.log(Level.INFO, "1 Multiplica 3 por 2 = {0}", multiply4.doMath(3,2));
//        LOGGER.log(Level.INFO, "1 Multiplica 3 por 2 = {0}", multiply4.doMath(null,2));
    }
}

@FunctionalInterface 
interface ITest { 
 public abstract double doMath (double x, double y);
}