package com.previred.java.presentacionjava11.thread;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mig_s
 */
public class Ejemplo01Thread {

    private static final Logger LOGGER = Logger.getLogger(Ejemplo01Thread.class.getName());

    public static void main(String args[]) {
        Thread t1 = new Thread(new MyClass());
        t1.start();
//        t1.destroy();
        while (t1.isAlive()) {            
            LOGGER.log(Level.INFO, "Thread MyClass running {0}", t1.isAlive());
        }
    }
}

class MyClass implements Runnable {
    private static final Logger LOGGER = Logger.getLogger(MyClass.class.getName());
    public void run() {
        LOGGER.info("MyClass running");
    }
}
