package com.previred.java.presentacionjava11.httpcliente;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mig_s
 */
public class Ejemplo03MultiplesRequest {

    private static final Logger LOGGER = Logger.getLogger(Ejemplo03MultiplesRequest.class.getName());

    public static void main(String args[]) throws IOException, InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(6);
        HttpClient httpClient = HttpClient.newBuilder()
                .version(Version.HTTP_1_1)
                .build();
        HttpRequest mainRequest = HttpRequest.newBuilder()
                .uri(URI.create("https://http2.akamai.com/demo/h2_demo_frame.html"))
                .build();

        HttpResponse<String> mainResponse = httpClient.send(mainRequest, BodyHandlers.ofString());

        List<Future<?>> futures = new ArrayList<>();
// For each image resource in the main HTML, send a request on a separate thread
        mainResponse.body().lines()
                .filter(line -> line.trim().startsWith("<img height"))
                .map(line -> line.substring(line.indexOf("src='") + 5, line.indexOf("'/>")))
                .forEach((String image) -> {
                    LOGGER.log(Level.INFO, "Imagenes: {0}", image);
                    Future imgFuture = executor.submit(() -> {
                        HttpRequest imgRequest = HttpRequest.newBuilder()
                                .uri(URI.create("https://http2.akamai.com" + image))
                                .build();
                        try {
                            HttpResponse<String> imageResponse = httpClient.send(imgRequest, BodyHandlers.ofString());
//                            LOGGER.log(Level.INFO, "Loaded {0}, status code: {1}", new Object[]{image, imageResponse.statusCode()});
                        } catch (IOException | InterruptedException ex) {
                            LOGGER.log(Level.SEVERE, "Error during image request for " + image, ex);
                        }
                    });
                    futures.add(imgFuture);
                });
        
        LOGGER.log(Level.INFO, "Cantidad de Futures: {0}", futures.size());
        
// Wait for all submitted image loads to be completed
        futures.forEach(f -> {
            try {
                f.get();
            } catch (InterruptedException | ExecutionException ex) {
                LOGGER.log(Level.SEVERE, "Error waiting for image load", ex);
            }
        });
    }
}
