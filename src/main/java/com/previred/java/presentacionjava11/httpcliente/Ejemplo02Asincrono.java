package com.previred.java.presentacionjava11.httpcliente;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mig_s
 */
public class Ejemplo02Asincrono {

    private static Object LOCK = new Object();
    private static final Logger LOGGER = Logger.getLogger(Ejemplo02Asincrono.class.getName());

    public static void main(String args[]) throws IOException, InterruptedException {
        var httpClient = HttpClient.newBuilder()
                .version(Version.HTTP_2) // este es el valor por defecto
                .build();

        var request = HttpRequest.newBuilder()
                .uri(URI.create("https://http2.github.io/"))
                .GET() // este es el valor por defecto
                .build();

        httpClient.
                sendAsync(request, BodyHandlers.ofString()).
                thenAccept(response -> {
                    LOGGER.log(Level.INFO, "Response status code: {0}", response.statusCode());
                    LOGGER.log(Level.INFO, "Response headers: {0}", response.headers());
                    LOGGER.log(Level.INFO, "Response body: {0}", response.body());
                });

        LOGGER.log(Level.INFO, "***** Ejecutando HTTP Get *****");
        Ejemplo02Asincrono.sleepWaitExamples();
    }

    private static void sleepWaitExamples()
            throws InterruptedException {

        Thread.sleep(1000);
        synchronized (LOCK) {
            LOCK.wait(1000);
            System.out.println("espera por 1 second");
        }
    }
}
