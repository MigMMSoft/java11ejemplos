package com.previred.java.presentacionjava11.pruebaProceso;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author MigSoft
 */
public class ProcesaArchivo {
    private static final int VECTOR = 6;
    private static final int MAX_VECTOR = 23;
    private static final int INICIO_CADENA = 9;
    private static final int LARGO_TOTAL = 148;
    
    //
    public static void main(String args[]) throws IOException {
        ProcesaArchivo p = new ProcesaArchivo();
        //p.procesar();
        String arch = "..\\..\\vector1000000.txt";
        Long inicio = System.nanoTime();
        p.procesaArchivo(arch);
        Long fin = System.nanoTime();
        System.out.println("Tiempo ocupado: " + TimeUnit.SECONDS.convert((fin-inicio), TimeUnit.NANOSECONDS));
    }
    
    public void procesaArchivo(String archivo) throws IOException{
        File salida = new File("procesado"+archivo);
        try (BufferedWriter output = new BufferedWriter(new FileWriter(salida))) {
            Consumer<String> c = (linea) -> {
                try {
                    output.write(linea);
                } catch (IOException ex) {
                    Logger.getLogger(ProcesaArchivo.class.getName()).log(Level.SEVERE, null, ex);
                }
            };
            
            Path path = FileSystems.getDefault().getPath(archivo);
            Files.lines(path)
//                .parallel()
                .map(ProcesaArchivo::procesarLine)
                .forEach(c);
        }
        
    }
    
    public static String procesarLine(String linea){
        //rescata secuencia
        var salida = new StringBuilder(linea.substring(0, INICIO_CADENA));
        //divide periodos
        var procesa = linea.substring(INICIO_CADENA);
        //divide periodos
        HashSet<String> slist = new HashSet();
//        var inicio = 0;
//        var fin = VECTOR;
//        while (procesa.length() > fin) {
//            String fecha = procesa.substring(inicio, fin);
//            inicio += VECTOR;
//            fin += VECTOR;
//            if(!"000000".equals(fecha))
//                slist.add(fecha);
//        }
        
        IntStream.
                iterate(0, i -> i<procesa.length(), i -> i + VECTOR).
//                parallel().
                filter(n -> !"000000".equals(procesa.substring(n, n+VECTOR))).
                forEach(n->slist.add(procesa.substring(n, n+VECTOR)));

        //califica
        if (slist.isEmpty()) {//sin elementos
            salida.append("N");
        }else if(slist.size() > MAX_VECTOR){
            salida.append("S");
        }else{
            salida.append("D");
            salida.append(slist.stream().sorted().collect(Collectors.joining()));
        }
        //rellena
        salida.append(" ".repeat(LARGO_TOTAL-salida.length())).append("\n");

        return salida.toString();
    }
}
