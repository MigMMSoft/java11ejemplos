package com.previred.java.presentacionjava11.string;

import java.util.logging.Logger;

/**
 * lines
 * @author mig_s
 */
public class Ejemplo01Line {
    private static final Logger LOGGER = Logger.getLogger(Ejemplo01Line.class.getName());

    public static void main(String args[]) {
        String mensaje = "uno \n dos \n tres \n cuato";
        
        mensaje.lines().forEach((msg) -> LOGGER.info(msg));
    }
}
