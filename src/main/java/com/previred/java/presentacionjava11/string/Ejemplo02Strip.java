package com.previred.java.presentacionjava11.string;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * String::strip, String::stripLeading, String::stripTrailing
 * @author mig_s
 */
public class Ejemplo02Strip {
    private static final Logger LOGGER = Logger.getLogger(Ejemplo02Strip.class.getName());

    public static void main(String args[]) {
        String dato = "  test dato  ";
        
        LOGGER.log(Level.INFO, "-{0}-", dato.strip());
        LOGGER.log(Level.INFO, "-{0}-", dato.stripLeading());
        LOGGER.log(Level.INFO, "-{0}-", dato.stripTrailing());
    }
}
