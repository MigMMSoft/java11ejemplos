package com.previred.java.presentacionjava11.string;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * isBlank
 * @author mig_s
 */
public class Ejemplo03IsBlank {
    private static final Logger LOGGER = Logger.getLogger(Ejemplo03IsBlank.class.getName());

    public static void main(String args[]) {
        String dato = "";
        
        LOGGER.log(Level.INFO, "Esta en Blanco: {0}", dato.isBlank());
        LOGGER.log(Level.INFO, "Esta Vacío: {0}", dato.isEmpty());
        
        dato = " ";
        
        LOGGER.log(Level.INFO, "Esta en Blanco: {0}", dato.isBlank());
        LOGGER.log(Level.INFO, "Esta Vacío: {0}", dato.isEmpty());
        
        dato = "      ";
        
        LOGGER.log(Level.INFO, "Esta en Blanco: {0}", dato.isBlank());
        LOGGER.log(Level.INFO, "Esta Vacío: {0}", dato.isEmpty());
    }
}
