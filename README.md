### Ejemplo Java 11

Proyecto de ejemplo de nuevas funcionalidades en java 11
Estos ejemplos son para la presentación que se encuentra en:
[Prezi java 9-10-11](https://prezi.com/view/RT84iTZK97WgbScY9b5R)

## Detalle de los sistemas

Java 11
Maven 3

## Detalle de Directorios

Los ejemplos contenidos están organizados de la siguiente forma:
Ejemplos de nuevas funcionalidades con Java 11

**java11ejemplos**
Ejemplos varios de nuevas implementaciones java 11 (\src\main\java\com\previred\java\presentacionjava11)
	* httpcliente
		* Ejemplo01HttpGet.java - Ejemplo de consumo de sito web usando http get
		* Ejemplo02Asincrono.java  - Ejemplo de consumo de sito web usando http get de forma asíncrona
		* Ejemplo03MultiplesRequest.java - Ejemplo de consumo de sito web usando http get múltiples
	* string
		* Ejemplo01Line.java - Ejemplo de uso de **line** en **string**
		* Ejemplo02Strip.java - Ejemplo de uso de **Strip** en **string**
		* Ejemplo03IsBlank.java - Ejemplo de uso de **IsBlank** en **string**
	* thread
		* Ejemplo01Thread.java - Ejemplo de eliminación de método deprecado
	* varLambdas
		* Ejemplo01Lambdas.java - Ejemplo de **var** en **lambdas**
	* pruebaProceso
		* ProcesaArchivo.java - Este es un ejemplo de procesamiento de un archivo de 1 millón de registros, esto es para comparar velocidad de procesamiento entre versiones de java.
